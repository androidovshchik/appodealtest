package com.gallera.appodealtestapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_inside.*
import kotlinx.android.synthetic.main.fragment_inside.view.*

class InsideFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_inside, container, false)
        val mainActivity = activity as MainActivity
        loadCard(view, mainActivity.choosedCard)
        initBackButton(mainActivity, view)
        return view
    }

    @SuppressLint("SetTextI18n")
    private fun loadCard(view: View, id: Int) {
        when (id) {
            1 -> {
                frInside_image.visibility = View.VISIBLE
                frInside_title.text = "dfg"
                frInside_DescriptionTitle.text = "dfg"
            }
            2 -> {
                frInside_title.text = "asd"
                frInside_DescriptionTitle.text = "asd"
            }
        }
    }

    private fun initBackButton(mainActivity: MainActivity, view: View) {
        view.frInside_BackBtn.setOnClickListener {
            mainActivity.onBackPressed()
        }
    }
}