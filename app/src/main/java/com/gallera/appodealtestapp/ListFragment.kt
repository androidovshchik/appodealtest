package com.gallera.appodealtestapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_list.view.*

class ListFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        val mainActivity = activity as MainActivity
        initButtons(mainActivity.choosedCard, view, mainActivity)

        view.frList_BackBtn.setOnClickListener {
            mainActivity.onBackPressed()
        }

        return view
    }

    fun initButtons(choosedCard: Int, view: View, mainActivity: MainActivity) {
        when (choosedCard) {
            4 -> {
                view.frList_card1.visibility = View.VISIBLE
                view.frList_card1.setOnClickListener {
                    mainActivity.choosedCard = 8
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
                view.frList_card2.visibility = View.VISIBLE
                view.frList_card2.setOnClickListener {
                    mainActivity.choosedCard = 9
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
                view.frList_card3.visibility = View.VISIBLE
                view.frList_card3.setOnClickListener {
                    mainActivity.choosedCard = 10
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
            }
            6 -> {
                view.frList_card1.visibility = View.VISIBLE
                view.frList_card1.setOnClickListener {
                    mainActivity.choosedCard = 11
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
                view.frList_card2.visibility = View.VISIBLE
                view.frList_card2.setOnClickListener {
                    mainActivity.choosedCard = 12
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
            }
            7 -> {
                view.frList_card1.visibility = View.VISIBLE
                view.frList_card1.setOnClickListener {
                    mainActivity.choosedCard = 11
                    mainActivity.loadFragment(MainActivity.fr.INSIDE)
                }
            }
        }
    }
}