package com.gallera.appodealtestapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.view.*

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val mainActivity = activity as MainActivity
        initButtons(view, mainActivity)
        return view
    }

    private fun initButtons(view: View, mainActivity: MainActivity) {
        view.frMain_card1.setOnClickListener {
            mainActivity.choosedCard = 1
            mainActivity.loadFragment(MainActivity.fr.INSIDE)
        }
        view.frMain_card2.setOnClickListener {
            mainActivity.choosedCard = 2
            mainActivity.loadFragment(MainActivity.fr.INSIDE)
        }
        view.frMain_card3.setOnClickListener {
            mainActivity.choosedCard = 3
            mainActivity.loadFragment(MainActivity.fr.INSIDE)
        }
        view.frMain_card4.setOnClickListener {
            mainActivity.choosedCard = 4
            mainActivity.loadFragment(MainActivity.fr.LIST)
        }
        view.frMain_card5.setOnClickListener {
            mainActivity.choosedCard = 5
            mainActivity.loadFragment(MainActivity.fr.INSIDE)
        }
        view.frMain_card6.setOnClickListener {
            mainActivity.choosedCard = 6
            mainActivity.loadFragment(MainActivity.fr.LIST)
        }
        view.frMain_card7.setOnClickListener {
            mainActivity.choosedCard = 7
            mainActivity.loadFragment(MainActivity.fr.INSIDE)
        }
        view.frMain_card8.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.appodeal.com/home/privacy-policy/")))
        }
    }

}