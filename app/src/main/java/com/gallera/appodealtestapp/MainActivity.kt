package com.gallera.appodealtestapp

import android.R.attr
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.appodeal.ads.Appodeal


class MainActivity : AppCompatActivity() {

    enum class fr { MAIN, INSIDE, LIST }

    var currentFragment = fr.MAIN
    var choosedCard = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadFragment(fr.MAIN)
        Appodeal.initialize(this, "d73c0e26ff5c94a94e27566a32344d68fa17f3bd2cdab95c", Appodeal.INTERSTITIAL, true)
    }

    fun loadFragment(id: fr) {
        lateinit var fragment: Fragment
        when (id) {
            fr.MAIN -> fragment = MainFragment()
            fr.INSIDE -> fragment = InsideFragment()
            fr.LIST -> fragment = ListFragment()
        }
        currentFragment = id
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_frameLayout, fragment)
            .commit()
    }

    override fun onBackPressed() {
        when (currentFragment){
            fr.MAIN -> super.onBackPressed()
            fr.INSIDE -> { showAd(); loadFragment(fr.MAIN); }
            fr.LIST -> { loadFragment(fr.MAIN) }
        }
    }

    fun showAd() {
        if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
            Appodeal.show(this, Appodeal.INTERSTITIAL)
        }
    }
}